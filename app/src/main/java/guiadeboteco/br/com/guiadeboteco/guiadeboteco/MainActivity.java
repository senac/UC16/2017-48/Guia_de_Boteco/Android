package guiadeboteco.br.com.guiadeboteco.guiadeboteco;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent() ;
        if(intent != null && intent.getSerializableExtra(BotecoActivity.PRATOS) != null ) {
            List<Pratos> lista = (List<Pratos>) intent.getSerializableExtra(BotecoActivity.PRATOS);

            listView = findViewById(R.id.ListaPratosID);
            ArrayAdapter adapter = new AdapterPratos(this, lista );
            listView.setAdapter(adapter);

        }

    }

}
