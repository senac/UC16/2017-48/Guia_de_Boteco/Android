package guiadeboteco.br.com.guiadeboteco.guiadeboteco;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BotecoActivity extends Activity {

    public static final String PRATOS = "pratos" ;

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.botecolista);

        listView = findViewById(R.id.ListabotecoID);
        ArrayAdapter adapter = new AdapterBoteco(this, adicionarBoteco());
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int posicao, long l) {

                Intent intent = new Intent(BotecoActivity.this, MainActivity.class) ;
                Boteco boteco = (Boteco) adapter.getItemAtPosition(posicao) ;
                Log.i("#BOTECO" , boteco.getDescricao());
                List<Pratos> lista = boteco.getListaPratos() ;

                if(lista.size() > 0 ) {
                    intent.putExtra(PRATOS, (Serializable) lista);
                    startActivity(intent);
                }else{
                    Toast.makeText(BotecoActivity.this , "Não Existem pratos disponiveis ainda" , Toast.LENGTH_LONG).show();
                }

            }
        });


    }

    private ArrayList<Boteco> adicionarBoteco() {
        ArrayList<Boteco> boteco = new ArrayList<Boteco>();
        Boteco e = new Boteco("De Passagem Botequim",
                "Lugar de lazer, alegria e amizade, com atendimento de 1ª. Deliciosos pratos, cerveja super gelada e música ao vivo. Rua Castelo Branco, 815, Praia da Costa - Vila Velha 2ª a 5ª das 18h à 0h. 6ª e sábado das 18h à 1h", R.drawable.depassagem);

        Pratos prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

        Pratos prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
        Pratos prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
        Pratos prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);










        boteco.add(e);
        e = new Boteco("Boteco Redentor",
                "Bar estilo rústico, com capacidade de 250 pessoas. Rua Carlos Gomes, 433, Parque Residencial Laranjeiras - Serra 3ª a sábado das 18h à 1h 99976-6223", R.drawable.redentor);

         prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

         prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
         prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
         prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);







        boteco.add(e);
        e = new Boteco("Boteco Presidente",
                "O Boteco Presidente fica em Cariacica e se destaca pelo seu cardápio com petiscos e drinks diferenciados. Rua Dom Pedro II, 01, Cruzeiro do Sul, Lj 1, Pavimento 01 - Cariacica 2ª a 4ª das 18h às 23h30. 5ª a sábado das 18h à 0h", R.drawable.botecopresidente);

        prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

        prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
        prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
        prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);





        boteco.add(e);
        e = new Boteco("Boteco Oratório",
                "Oferecemos ambiente familiar, com espaço Kids e refeição a moda a la carte. Especializado em peixes e mariscos em geral, também temos carnes, petiscos e massas. Música ao vivo sexta e sábado. Rua Humberto Pereira, 11, Itaparica - Vila Velha 2ª a 6ª das 17h à 0h. Sábado, domingo e feriado das 11h à 0h 3072-6070", R.drawable.botecooratorio);


        prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

        prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
        prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
        prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);


        boteco.add(e);
        e = new Boteco("Bar dos Cumpadres",
                "Lugar aonde frequentam os melhores clientes do mundo, servidos com simpatia e qualidade. Rua Doutor Jair de Andrade, 434, Lj 1/2, Itapuã - Vila Velha 3ª a 6ª das 17h30 à 0h. Sábado das 10h à 0h. Domingo das 10h às 19h 3062-6124", R.drawable.barcumpadres);


        prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

        prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
        prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
        prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);



        boteco.add(e);
        e = new Boteco("BarZito", "Desde 1977 na Praia da Costa em Vila Velha, procurando trazer o melhor da gastronomia de boteco e com ótimas bebidas. Rua Lúcio Barcelar, 16, Lj 7, Edifício Sereia, Praia da Costa - Vila Velha 2ª a 6ª das 17h às 23h. Sábado das 11h às 18h", R.drawable.barzito);

        prato = new Pratos("Bolinho de Baião de dois",
                "Um dos maiores desafios que meu grupo se propôs no último trabalho semestral foi transformar pratos típicos de determinadas regiões do Brasil em Comida de Boteco. Em alguns casos, buscamos pratos tradicionais, em outros fomos atrás de ingredientes ou técnicas de preparo, sempre transformando o preparo naquelas comidas que esperamos encontrar em um boteco no final de tarde, para esquecer nossos problemas, socializar com os amigos, escutar e contar histórias. Trago então para vocês a primeira das criações.", R.drawable.image1);
        e.adicionarPrato(prato);

        prato2  = new Pratos("Bolinho de Bacalhau",
                "O bacalhau é um alimento milenar e que revolucionou a alimentação humana por conta da fácil conservação, já que era salgado e ainda mantinha seus nutrientes mesmo depois do corte e do salgamento do produto. A primeira receita oficial do famoso bolinho de bacalhau foi em 1904 e saiu num livro de receitas chamado Tratado de Cozinha e Copa de Carlos Bandeira de Melo, oficial do exército português. Sendo assim, o bolinho tem origem tipicamente portuguesa.", R.drawable.image2);
        e.adicionarPrato(prato2);
        prato3  = new Pratos("Bolinho dos Cumpadres",
                "Deliciosos bolinhos de carne de boi e porco recheados com queijo e molho de pimenta biquinho.", R.drawable.image3);
        e.adicionarPrato(prato3);
        prato4 = new Pratos("Chameguinho",
                "Moqueca de camarão, carne seca com abóbora, purê de aipim e abóbora, massa de pastel, requeijão e muçarela.", R.drawable.image4);
        e.adicionarPrato(prato4);



        boteco.add(e);

        return boteco;
    }


}


