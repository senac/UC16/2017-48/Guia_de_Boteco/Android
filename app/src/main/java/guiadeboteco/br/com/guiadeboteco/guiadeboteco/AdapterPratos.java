package guiadeboteco.br.com.guiadeboteco.guiadeboteco;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterPratos extends ArrayAdapter<Pratos> {

    private final Context context;
    private final List<Pratos> elementos;
    public AdapterPratos(Context context, List<Pratos> elementos) {
        super(context, R.layout.pratos, elementos);
        this.context = context;
        this.elementos = elementos;

        }
        @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.pratos, parent, false);

        TextView nomePratos = (TextView) rowView.findViewById(R.id.nome);
        TextView descricao = (TextView) rowView.findViewById(R.id.descricao);
        ImageView imagem = (ImageView) rowView.findViewById(R.id.imagem);

        nomePratos.setText(elementos.get(position).getNome());
        descricao.setText(elementos.get(position).getDescricao());
        imagem.setImageResource(elementos.get(position).getImagem());
        return rowView;
    }
}
