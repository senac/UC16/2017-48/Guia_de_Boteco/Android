package guiadeboteco.br.com.guiadeboteco.guiadeboteco;

import java.util.ArrayList;
import java.util.List;

public class Boteco {

    private String nome;
    private String descricao;
    private int imagem;

    private List<Pratos> listaPratos;

    public Boteco(String nome, String descricao, int imagem) {
        this.nome = nome;
        this.descricao = descricao;
        this.imagem = imagem;
        this.listaPratos = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public void adicionarPrato(Pratos pratos){
        this.listaPratos.add(pratos);
    }

    public void removerPrato(Pratos pratos){
        this.listaPratos.remove(pratos);
    }

    public List<Pratos> getListaPratos() {
        return listaPratos;
    }
}